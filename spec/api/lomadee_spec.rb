require 'spec_helper'
require 'ciclo_afiliado'

describe CicloAfiliado::API::Lomadee do
  subject { CicloAfiliado::API::Lomadee }

  context ".configure" do
    it "should configure correct" do
      expect( subject.config.app_token   ).to be_nil
      expect( subject.config.language    ).to eq(:BR)
      expect( subject.config.envirolment ).to eq(:development)
      expect( subject.config.base_url    ).to eq("http://sandbox.buscape.com.br")

      subject.configure do |config|
        config.app_token = 'token'
        config.language  = :EN
        subject.config.envirolment = :production
      end

      expect( subject.config.app_token   ).to eq('token')
      expect( subject.config.language    ).to eq(:EN)
      expect( subject.config.envirolment ).to eq(:production)
      expect( subject.config.base_url    ).to eq("http://bws.buscape.com.br")
    end
  end
end