require 'spec_helper'
require 'ciclo_afiliado'

module CicloAfiliado::API::Zanox
  describe Categoria do
    subject { Categoria }

    let(:stubs  ) { Faraday::Adapter::Test::Stubs.new                      }
    let(:conexao) { Faraday.new { |builder| builder.adapter :test, stubs } }

    let(:categories_with_success) { read_file('spec/requests/api/zanox/programa/fetch_categorias.json') }

    before(:all) do
      CicloAfiliado::API::Zanox.configure do |config|
        config.app_token   = ENV['ZANOX_SECRET_KEY']
        config.source_id   = ENV['ZANOX_CONNECT_ID']
        config.language    = :BR
        config.per_page    = 10
      end
    end

    after(:all) do
      CicloAfiliado::API::Zanox.set_default
    end

    describe "#fetch" do
      context "when have categories" do
        it "should return correct categories" do
          proxy = subject.new({
            program_id: '123'
          })

          allow(proxy).to receive(:conexao) { conexao }
          stubs.get( proxy.path ) { |env| [ 200, {}, categories_with_success ]}

          categoria_collection = proxy.fetch

          expect(proxy.total).to eq(31)
          expect(proxy.pagina).to eq(1)
          expect(categoria_collection).to be_a(CicloAfiliado::Genericos::CategoriaCollection)
          expect(categoria_collection.count).to eq(31)
        end
      end
    end
  end
end