require 'spec_helper'
require 'ciclo_afiliado'

module CicloAfiliado::API::Zanox
  describe Produto do
    subject { Produto }

    let(:fetch_com_duas_paginas) { read_file('spec/requests/api/zanox/produto/fetch_com_duas_paginas.json') }
    let(:fetch_api) { read_file('spec/requests/api/zanox/produto/fetch.json') }

    before(:all) do
      CicloAfiliado::API::Zanox.configure do |config|
        config.app_token   = ENV['ZANOX_SECRET_KEY']
        config.source_id   = ENV['ZANOX_CONNECT_ID']
        config.language    = :BR
        config.per_page    = 10
      end
    end

    after(:all) do
      CicloAfiliado::API::Zanox.set_default
    end

    context "#fetch" do
      let(:stubs  ) { Faraday::Adapter::Test::Stubs.new                      }
      let(:conexao) { Faraday.new { |builder| builder.adapter :test, stubs } }

      it "retornando os top produtos" do
        proxy = subject.new

        allow(proxy).to receive(:conexao) { conexao }
        stubs.get( proxy.path ) { |env| [ 200, {}, fetch_api ]}

        produto_collection = proxy.fetch

        expect(proxy.total).to eq(896)
        expect(proxy.pagina).to eq(1)
        expect(produto_collection).to be_a(CicloAfiliado::Genericos::ProdutoCollection)
        expect(produto_collection.count).to eq(10)
      end

      it "deve tornar os produtos preenchidos corretamente" do
        proxy = subject.new

        allow(proxy).to receive(:conexao) { conexao }
        stubs.get( proxy.path ) { |env| [ 200, {}, fetch_api ]}

        produto_collection = proxy.fetch

        produto = produto_collection.first

        expect(produto.id).to eq("57826eec0f00421997de9b238737dc43")
        expect(produto.nome).to eq("tenis")
        expect(produto.programa).to eq("Livraria Cultura BR")
        expect(produto.programa_id).to eq(13940)
        expect(produto.preco).to eq(24.5)
        expect(produto.image).to eq("http://statics.livrariacultura.net.br/products/capas_lg/835/675835.jpg")
        expect(produto.url).to eq('http://ad.zanox.com/ppc/?35514031C502052058&ULP=[[Cooler-Universal-Intel-e-AMD/4523-4533-4564-452399/?utm_source=Zanox&prc=8803&utm_medium=CPC_Acessorios_de_Informatica_Zanox&utm_campaign=Componentes_e_Pecas&utm_content=Coolers_e_Ventiladores&cda=DA68-7C11-84BC-5FE1]]&zpar9=[[49FBEB147C6A5EDC9F97]]')
      end
    end

    context "#next" do
      let(:stubs  ) { Faraday::Adapter::Test::Stubs.new                      }
      let(:conexao) { Faraday.new { |builder| builder.adapter :test, stubs } }

      it "deve ir para o próximo indice da perquisa" do
        produto = subject.new

        allow(produto).to receive(:conexao) { conexao }
        stubs.get( produto.path ) { |env| [ 200, {}, fetch_com_duas_paginas ]}

        produto.fetch
        expect { produto.next }.to change { produto.pagina }.from(1).to(2)
      end

      it "deve retornar nil quando não tiver mais como avançar" do
        produto = subject.new

        allow(produto).to receive(:conexao) { conexao }
        stubs.get( produto.path ) { |env| [ 200, {}, fetch_com_duas_paginas ]}
        
        produto.fetch
        produto.next
        expect(produto.pagina).to eq(2)
        expect(produto.total).to eq(2)
        expect(produto.next).to be_nil
      end
    end

    context "#prev" do
      let(:stubs  ) { Faraday::Adapter::Test::Stubs.new                      }
      let(:conexao) { Faraday.new { |builder| builder.adapter :test, stubs } }

      it "deve ir para a pagina anterior" do
        produto = subject.new

        allow(produto).to receive(:conexao) { conexao }
        stubs.get( produto.path ) { |env| [ 200, {}, fetch_com_duas_paginas ]}
        
        produto.fetch
        produto.next

        expect { produto.prev }.to change { produto.pagina }.from(2).to(1)
      end

      it "deve retornar nil quando não tiver mais como recuar" do
        produto = subject.new

        allow(produto).to receive(:conexao) { conexao }
        stubs.get( produto.path ) { |env| [ 200, {}, fetch_com_duas_paginas ]}

        produto.fetch
        expect(produto.pagina).to eq(1)
        expect(produto.prev).to be_nil
      end
    end
  end
end