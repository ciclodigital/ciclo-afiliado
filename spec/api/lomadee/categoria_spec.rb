require 'spec_helper'
require 'ciclo_afiliado'

describe CicloAfiliado::API::Lomadee::Categoria do
  subject { CicloAfiliado::API::Lomadee::Categoria }
  let(:stubs) { Faraday::Adapter::Test::Stubs.new }
  let(:conexao) do
    Faraday.new do |builder|
      builder.adapter :test, stubs
    end
  end

  before do
    CicloAfiliado::API::Lomadee.configure do |config|
      config.app_token = '1234'
      config.source_id  = '1234'
      config.format    = :json
    end
  end

  after do
    CicloAfiliado::API::Lomadee.set_default
  end

  context ".listar" do
    let(:response_api) { read_file('spec/requests/api/lomadee/categoria/listar.json') }

    it "Deve retornar um CategoriaCollection" do
      allow(subject).to receive(:conexao) { conexao }
      stubs.get( subject.url_path ) { |env| [ 200, {}, response_api ]}
      
      collection = subject.pesquisar "smartphone"

      expect(collection).to be_a(CicloAfiliado::Genericos::CategoriaCollection)

      stubs.verify_stubbed_calls
    end

    it "Deve retornar uma lista com categorias" do
      allow(subject).to receive(:conexao) { conexao }

      stubs.get( subject.url_path ) { |env| [ 200, {}, response_api ]}

      expect(subject.pesquisar("smartphone").length).to eq(39)
      
      stubs.verify_stubbed_calls
    end

    it "Deve retornar as categorais do tipo CicloAfiliado::Genericos::Categoria" do
      allow(subject).to receive(:conexao) { conexao }

      stubs.get( subject.url_path ) { |env| [ 200, {}, response_api ]}

      expect(subject.pesquisar("smartphone").first).to be_a(CicloAfiliado::Genericos::Categoria)
      
      stubs.verify_stubbed_calls
    end
  end

end