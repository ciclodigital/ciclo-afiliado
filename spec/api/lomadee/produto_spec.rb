require 'spec_helper'
require 'ciclo_afiliado'

describe CicloAfiliado::API::Lomadee::Produto do
  subject { CicloAfiliado::API::Lomadee::Produto }
  
  let(:fetch_com_duas_paginas) { read_file('spec/requests/api/lomadee/produto/fetch_com_duas_paginas.json') }
  let(:response_api) { read_file('spec/requests/api/lomadee/produto/fetch.json') }
  let(:fetch_empty_products) { read_file('spec/requests/api/lomadee/produto/fetch_empty_products.json') }

  let(:stubs  ) { Faraday::Adapter::Test::Stubs.new                      }
  let(:conexao) { Faraday.new { |builder| builder.adapter :test, stubs } }

  before(:all) do
    CicloAfiliado::API::Lomadee.configure do |config|
      config.app_token   = ENV['LOMADEE_APP_TOKEN']
      config.source_id   = ENV['LOMADEE_SOURCE_ID']
      config.language    = :BR
      config.envirolment = :development
      config.per_page    = 10
    end
  end

  after(:all) do
    CicloAfiliado::API::Lomadee.set_default
  end

  describe "#fetch" do
    context "when have products" do

      it "retornando os top produtos" do
        produto = subject.new

        allow(produto).to receive(:conexao) { conexao }
        stubs.get( produto.path ) { |env| [ 200, {}, response_api ]}

        produto_collection = produto.fetch

        expect(produto.total).to eq(145)
        expect(produto.pagina).to eq(1)
        expect(produto_collection).to be_a(CicloAfiliado::Genericos::ProdutoCollection)
        expect(produto_collection.count).to eq(10)
      end
    end

    context "when paginate without limit of products" do
      it "should return empty produto_collection" do
        produto = subject.new

        allow(produto).to receive(:conexao) { conexao }
        stubs.get( produto.path ) { |env| [ 200, {}, fetch_empty_products ]}

        produto_collection = produto.fetch

        expect(produto.total).to eq(0)
        expect(produto.pagina).to eq(1)
        expect(produto_collection).to be_a(CicloAfiliado::Genericos::ProdutoCollection)
        expect(produto_collection.count).to eq(0)
      end
    end
  end

  context "#next" do
    it "deve ir para o próximo indice da perquisa" do
      produto = subject.new

      allow(produto).to receive(:conexao) { conexao }
      stubs.get( produto.path ) { |env| [ 200, {}, fetch_com_duas_paginas ]}

      produto.fetch
      expect { produto.next }.to change { produto.pagina }.from(1).to(2)
    end

    it "deve retornar nil quando não tiver mais como avançar" do
      produto = subject.new

      allow(produto).to receive(:conexao) { conexao }
      stubs.get( produto.path ) { |env| [ 200, {}, fetch_com_duas_paginas ]}
      
      produto.fetch
      produto.next
      expect(produto.pagina).to eq(2)
      expect(produto.total).to eq(2)
      expect(produto.next).to be_nil
    end
  end

  context "#prev" do
    it "deve ir para a pagina anterior" do
      produto = subject.new

      allow(produto).to receive(:conexao) { conexao }
      stubs.get( produto.path ) { |env| [ 200, {}, fetch_com_duas_paginas ]}
      
      produto.fetch
      produto.next

      expect { produto.prev }.to change { produto.pagina }.from(2).to(1)
    end

    it "deve retornar nil quando não tiver mais como recuar" do
      produto = subject.new

      allow(produto).to receive(:conexao) { conexao }
      stubs.get( produto.path ) { |env| [ 200, {}, fetch_com_duas_paginas ]}

      produto.fetch
      expect(produto.pagina).to eq(1)
      expect(produto.prev).to be_nil
    end
  end
end