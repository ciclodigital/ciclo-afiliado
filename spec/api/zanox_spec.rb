require 'spec_helper'
require 'ciclo_afiliado'

module CicloAfiliado::API
  describe Zanox do
    subject { Zanox }

    context ".configure" do
      it "should configure correct" do
        expect( subject.config.app_token   ).to be_nil
        expect( subject.config.language    ).to eq(:BR)
        expect( subject.config.base_url    ).to eq("http://api.zanox.com")
      end
    end
  end
end