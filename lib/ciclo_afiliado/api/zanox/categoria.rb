module CicloAfiliado
  module API
    module Zanox
      class Categoria < ::CicloAfiliado::API::Base::Request
        def initialize params = {}
          @pagina = 1
          @total  = -1
          @cache  = []
          @config = Zanox.config
          @params = params
        end

        def path
          "/#{@config.format}/2011-03-01/products/merchantcategories"
        end

        private
          def parse hash
            collection = Genericos::CategoriaCollection.new

            categories = hash['categories'] || []
            categories = categories.first   || {}
            categories = categories['category'] || []
            categories = categories.is_a?(String) ? [categories] : categories

            categories.each do |cat|
              categoria  = parse_categoria(cat, collection.size)
              collection << categoria unless categoria.nil?
            end

            @total = collection.count
            
            collection
          end

          def update_total_pages hash
            @total = 1
          end

          def params
            {
              :connectid   => @config.source_id
            }.merge(@params)
          end

          def parse_categoria nome, size
            categoria = CicloAfiliado::Genericos::Categoria.new
            categoria.id       = "#{@params[:program]}_#{size}"
            categoria.nome     = nome
            categoria.afiliado = :zanox
            categoria
          end

      end
    end
  end
end
