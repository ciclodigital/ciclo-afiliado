module CicloAfiliado
  module API
    module Zanox
      class Produto < ::CicloAfiliado::API::Base::Request
        def initialize params = {}
          @pagina = 1
          @total  = -1
          @cache  = []
          @config = Zanox.config
          @params = params
        end

        def path
          "/#{@config.format}/2011-03-01/products"
        end

        def path_one product_id
          "/#{@config.format}/2011-03-01/products/product/#{product_id}"
        end

        def find product_id
          produto_hash = ::JSON.parse(conexao.get(path_one(product_id), {
            :connectid => @config.source_id
          }).body)
          
          if produto_hash.include?('productItem')
            parse_one(produto_hash['productItem'].first)
          else
            nil
          end
        end

        private
          def conexao
            @conexao = ::Faraday.new(:url => @config.base_url, request: { :params_encoder => Faraday::FlatParamsEncoder }) do |faraday|
              faraday.response :logger
              faraday.adapter  ::Faraday.default_adapter
            end

            @conexao
          end
          
          def parse hash
            collection = Genericos::ProdutoCollection.new

            produtos = []
            produtos = hash['productItems']['productItem'] if hash['productItems'].is_a?(Hash) && hash['productItems'].include?('productItem')

            return collection unless produtos.any?

            produtos.each do |produto_hash|
              collection << parse_one(produto_hash)
            end

            collection
          end

          def parse_one produto_hash
            produto = Genericos::Produto.new(:zanox)

            produto.id          = produto_hash['@id']
            produto.nome        = produto_hash['name']
            produto.preco       = produto_hash['price'].to_f
            produto.image       = parse_image(produto_hash)
            produto.programa    = produto_hash['program']['$']
            produto.programa_id = produto_hash['program']['@id'].to_i
            produto.loja.id     = produto.programa_id
            produto.loja.nome   = produto.programa
            produto.url         = parse_url(produto_hash)
            produto.marca       = produto_hash['manufacturer']
            produto.descricao   = produto_hash['description']

            produto
          end

          def update_total_pages hash
            if hash['total'].to_i > 0 && hash['items'].to_i > 0
              @total = hash['total'] / hash['items']
            else
              @total = hash['total'].to_i > 0 ? 1 : 0
            end
          end

          def params
            new_params = {
              :hasimages   => true,
              :connectid   => @config.source_id,
              :region      => @config.language,
              :items       => @config.per_page,
              :page        => @pagina - 1 # a zanox inicia sua paginação em 0
            }.merge(normalize_params(@params))

            new_params
          end

          def parse_url p
            if p && p.include?('trackingLinks') && p['trackingLinks'].class != String
              if p['trackingLinks'].include?('trackingLink')
                p['trackingLinks']['trackingLink'].first['ppc']
              end
            end
          end

          def parse_image p
            if p && p.include?('image')
              return p['image']['large']  if p['image'].include?('large')
              return p['image']['medium'] if p['image'].include?('medium')
              return p['image']['small']  if p['image'].include?('small')
            end
          end

          def normalize_params params
            if params && params.include?(:page)
              params[:page] = params[:page].to_i - 1
            end
            params
          end
      end
    end
  end
end
