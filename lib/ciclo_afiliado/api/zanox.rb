require 'ciclo_afiliado/api/zanox/produto'
require 'ciclo_afiliado/api/zanox/categoria'

module CicloAfiliado
  module API
    module Zanox
      class << self
        attr_accessor :configuration
      end

      def self.configure
        self.configuration ||= Configuration.new
        yield(configuration)
      end

      def self.set_default
        @configuration = Configuration.new
      end

      class Configuration
        attr_accessor :app_token
        attr_accessor :language
        attr_accessor :source_id
        attr_accessor :per_page
        attr_accessor :page
        attr_accessor :format

        def initialize
          @language    = :BR
          @app_token   = nil
          @source_id   = nil

          ##
          # Results
          @page        = 1
          @per_page    = 20
          @format      = :json # || :xml
        end

        def base_url
          "http://api.zanox.com"
        end
      end

      def self.config
        self.configuration ||= Configuration.new
      end
    end
  end
end
