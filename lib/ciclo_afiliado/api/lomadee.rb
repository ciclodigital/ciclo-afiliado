require 'ciclo_afiliado/api/lomadee/produto'
require 'ciclo_afiliado/api/lomadee/categoria'
require 'ciclo_afiliado/api/lomadee/oferta'
require 'ciclo_afiliado/api/lomadee/loja'

module CicloAfiliado
  module API
    module Lomadee
      class << self
        attr_accessor :configuration
      end

      def self.configure
        self.configuration ||= Configuration.new
        yield(configuration)
      end

      class Configuration
        attr_accessor :app_token
        attr_accessor :language
        attr_accessor :envirolment
        attr_accessor :source_id
        attr_accessor :per_page
        attr_accessor :page
        attr_accessor :format

        def initialize
          @language    = :BR
          @app_token   = nil
          @source_id   = nil
          @envirolment = :development # || :production

          ##
          # Results
          @page        = 1
          @per_page    = 10
          @format      = :json # || :xml
        end

        def base_url
          env = :sandbox
          env = :bws     if @envirolment == :production
          "http://#{env}.buscape.com.br"
        end
      end

      def self.config
        self.configuration ||= Configuration.new
      end

      def self.set_default
        self.configuration = Configuration.new
      end
    end
  end
end
