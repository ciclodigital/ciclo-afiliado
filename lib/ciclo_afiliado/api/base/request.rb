module CicloAfiliado
  module API
    module Base
      class Request
        attr_reader :total, :pagina, :cache
        
        def fetch
          return @cache[@pagina] if @cache[@pagina] #pode existir um cache

          hash = ::JSON.parse(conexao.get(path, params).body)
          
          update_total_pages(hash)

          @cache[@pagina] = parse(hash)

          @cache[@pagina]
        end

        def next
          if @pagina < @total || @total == -1
            @pagina += 1 if @total != -1 # somente se total já tiver sido inicializado
            fetch
          else # Não a mais páginas para percorrer
            nil
          end
        end

        def prev
          if @pagina > 1
            @pagina -= 1
            fetch
          else # Não a mais páginas para percorrer
            nil
          end
        end

        def path
          raise NotImplementedError, "method #path should be implemented"
        end

        protected
          def conexao
            @conexao = ::Faraday.new(:url => @config.base_url) do |faraday|
              faraday.response :logger
              faraday.adapter  ::Faraday.default_adapter
            end

            @conexao
          end

          def parse hash
            raise NotImplementedError, "method #parse should be implemented"
          end

          def update_total_pages hash
            raise NotImplementedError, "method #update_total_pages should be implemented"
          end

          def params
            raise NotImplementedError, "method #params should be implemented"
          end
      end
    end
  end
end
