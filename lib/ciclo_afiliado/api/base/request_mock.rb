module CicloAfiliado
  module API
    module Base
      class RequestMock < Request
        def initialize total, pagina
          @total  = total
          @pagina = pagina
        end
      end
    end
  end
end
