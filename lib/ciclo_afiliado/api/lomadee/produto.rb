require 'faraday'
require 'json'

module CicloAfiliado
  module API
    module Lomadee
      class Produto < ::CicloAfiliado::API::Base::Request
        
        def initialize params = {}
          @pagina = 1
          @total  = -1
          @cache  = []
          @config = Lomadee.config
          @params = params
        end

        def path
          "#{@config.base_url}/service/findProductList/lomadee/#{@config.app_token}/#{@config.language}/"
        end

        private
          def update_total_pages hash
            @total = hash['totalpages'].to_i if @total < 0 
          end

          def params
            {
              page: @pagina,
              sourceId: @config.source_id,
              format:   @config.format
            }.merge(@params)
          end

          def parse response_hash
            collection = Genericos::ProdutoCollection.new

            products = response_hash['product'] || []

            products.each do |p|
              p = p['product']
              produto = Genericos::Produto.new(:lomadee)

              produto.id = p['id']
              produto.nome =  p['productname']
              produto.image =  parse_image(p)
              produto.preco =  p['pricemin'].to_f
              produto.categoria_id =  p['categoryid']
              produto.url = get_link_by_type(p['links'], 'product')

              collection << produto
            end

            collection
          end

          def get_link_by_type links, type
            url = nil

            links.each do |l|
              url = l['link']['url'] if l['link']['type'] == type
            end

            url
          end

          def parse_image p
            if p.include?('thumbnail')
              if p['thumbnail'].include?('url')
                p['thumbnail']['url']
              elsif p['thumbnail'].include?('formats')
                p['thumbnail']['formats'].first['formats']['url']
              end
            end
          end
      end
    end
  end
end