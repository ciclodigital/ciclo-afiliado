require 'faraday'
require 'json'

module CicloAfiliado
  module API
    module Lomadee
      class Oferta < ::CicloAfiliado::API::Base::Request
        
        def initialize params = {}
          @pagina = 1
          @total  = -1
          @cache  = []
          @config = Lomadee.config
          @params = params
        end

        def path
          "#{@config.base_url}/service/v2/topOffers/lomadee/#{@config.app_token}/#{@config.language}/"
        end

        private
          def update_total_pages hash
            @total = hash['totalPages'].to_i if @total < 0 
          end

          def params
            {
              page: @pagina,
              sourceId: @config.source_id,
              format:   @config.format
            }.merge(@params)
          end

          def parse hash
            collection = Genericos::ProdutoCollection.new

            if hash.include? 'offer'
              hash['offer'].each do |offer|
                produto = Genericos::Produto.new(:lomadee)
                seller  = offer['seller']

                produto.id = offer['productId']
                produto.nome =  offer['offerName']
                produto.image =  get_imagem(offer)
                produto.de    = offer['priceFromValue'].to_f
                produto.preco =  offer['priceValue'].to_f
                produto.categoria_id =  offer['categoryId']
                produto.url = get_link_by_type(offer['links'], 'offer')
                produto.desconto = offer['discountPercent'].to_f
                produto.loja.id = seller['id']
                produto.loja.nome = seller['sellerName']
                produto.loja.image = seller['thumbnail']['url'] if seller.include?('thumbnail') && seller['thumbnail'].include?('url')

                collection << produto
              end
            end

            collection
          end

          def get_link_by_type links, type
            url = nil

            links['link'].each do |l|
              url = l['url'] if l['type'] == type
            end

            url
          end

          def get_imagem offer
            if offer.include?('thumbnail') && offer['thumbnail'].include?('url')
              offer['thumbnail']['url']
            else
              ''
            end
          end
      end
    end
  end
end