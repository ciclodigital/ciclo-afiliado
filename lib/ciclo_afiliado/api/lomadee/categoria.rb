require 'faraday'
require 'json'

module CicloAfiliado
  module API
    module Lomadee
      module Categoria
        @@conexao = nil

        def self.url_path
          "/service/findCategoryList/lomadee/#{Lomadee.config.app_token}/#{Lomadee.config.language}"
        end

        ##
        # 
        def self.pesquisar texto
          response = conexao.get( url_path , {
            sourceId: Lomadee.config.source_id,
            format: Lomadee.config.format,
            keyword: texto
          })

          criar_categorias_pelo_response(response)
        end

        private
          def self.conexao
            @@conexao if @conexao

            @@conexao = ::Faraday.new(:url => Lomadee.config.base_url) do |faraday|
              faraday.response :logger
              faraday.adapter  ::Faraday.default_adapter
            end

            @@conexao
          end

          def self.criar_categorias_pelo_response response

            collection = CicloAfiliado::Genericos::CategoriaCollection.new

            if response.status >= 200 && response.status < 300
              json       = ::JSON.parse(response.body)
              json['subcategory'].each do |c|
                collection << criar_categoria_pelo_response(c['subcategory'])
              end
            end

            collection
          end

          def self.criar_categoria_pelo_response api
            categoria = CicloAfiliado::Genericos::Categoria.new
            categoria.id        = api["id"]
            categoria.nome      = api["name"]
            categoria.thumbnail = api["thumbnail"]["url"]
            categoria.afiliado  = :lomadee
            categoria
          end
      end
    end
  end
end