require 'faraday'
require 'json'

module CicloAfiliado
  module API
    module Lomadee
      class Loja < ::CicloAfiliado::API::Base::Request
        
        def initialize params = {}
          @pagina = 1
          @total  = -1
          @cache  = []
          @config = Lomadee.config
          @params = params
        end

        def path
          "#{@config.base_url}/service/sellers/lomadee/#{@config.app_token}/#{@config.language}/"
        end

        private
          def update_total_pages hash
            @total = hash['totalpages'].to_i if @total < 0 
          end

          def params
            {
              sourceId: @config.source_id,
              format:   @config.format
            }
          end

          def parse hash
            collection = Genericos::ProdutoCollection.new

            hash['sellers'].each do |l|
              loja = Genericos::Loja.new

              produto.id    = p['id']
              produto.nome  = p['offerName']
              produto.image = p['thumbnail']['url'] unless p['thumbnail']['url'].nil?

              collection << loja
            end

            collection
          end
      end
    end
  end
end