module CicloAfiliado
  module Genericos
    class Loja
      attr_accessor :id, :nome, :image

      def initialize
        @image = 'https://s3-sa-east-1.amazonaws.com/descontopratudo/site/production/public/outhers/default.png'
      end
    end
  end
end
