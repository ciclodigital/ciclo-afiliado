module CicloAfiliado
  module Genericos
    class Produto
      attr_accessor :id, :nome, :url, :descricao, :image, :preco, :de, :desconto, :marca, :categoria_id, :afiliado, :programa, :programa_id, :loja

      def initialize afiliado = nil
        @afiliado = afiliado
        @loja     = ::CicloAfiliado::Genericos::Loja.new
      end
    end
  end
end
