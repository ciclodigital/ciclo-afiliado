# CicloAfiliado

Esta gem tem por objetivo ser uma abstração das APIs de afiliados.

## Afiliados

- Lomadee
- Zanox

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ciclo_afiliado'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ciclo_afiliado

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

